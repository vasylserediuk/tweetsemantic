using System.Text;
using RabbitMQ.Client;

namespace TweetSemantic
{
    public static class Rabbit
    {
        public static void SendToRabbit(string data)
        {
            var rabbitCfg = Program.Cfg.RabbitMq;
            var factory = new ConnectionFactory()
            {
                HostName = "localhost"
            };
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.QueueDeclare(queue: rabbitCfg.QueueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var body = Encoding.UTF8.GetBytes(data);

            channel.BasicPublish(exchange: "",
                rabbitCfg.QueueName,
                basicProperties: null,
                body: body);
        }
    }
}