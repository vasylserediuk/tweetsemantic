namespace TweetSemantic
{
    public class ApiOptions
    {
        public string RequestUri { get; set; }
        public string ApiKey { get; set; }
        public string ApiHost { get; set; }
    }
}