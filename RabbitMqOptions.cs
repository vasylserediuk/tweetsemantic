namespace TweetSemantic
{
    public class RabbitMqOptions
    {
        public string QueueName { get; set; }
    }
}