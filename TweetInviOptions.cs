namespace TweetSemantic
{
    public class TweetInviOptions
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string AccessToken { get; set; }
        public string AccessTokenSecret { get; set; }
        public long[] FollowerId { get; set; }
    }
}