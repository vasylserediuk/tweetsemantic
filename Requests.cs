using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Serilog;

namespace TweetSemantic
{
    public static class Requests
    {
        internal static async Task<dynamic> GetTweetPolarity(string tweet)
        {
            var api = Program.Cfg.RapidApi.Aylien;
            var query = System.Web.HttpUtility.ParseQueryString(string.Empty);
            query["text"] = tweet;
            query["mode"] = "tweet";
            return JsonConvert.DeserializeObject(await RequestToRapidApi(api, query));
        }

        private static async Task<dynamic> GetStockPrice(string nasdaq)
        {
            var api = Program.Cfg.RapidApi.StockPrice;
            var query = System.Web.HttpUtility.ParseQueryString(nasdaq);
            return JsonConvert.DeserializeObject(await RequestToRapidApi(api, query));
        }

        private static async Task<string> RequestToRapidApi(ApiOptions cfg, NameValueCollection query)
        {
            try
            {
                using var client = new HttpClient();
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(
                        cfg.RequestUri + query),
                    Headers =
                    {
                        {"x-rapidapi-key", cfg.ApiKey},
                        {"x-rapidapi-host", cfg.ApiHost},
                    }
                };
                using var response = await client.SendAsync(request);
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();
                return body;
            }
            catch (HttpRequestException exception)
            {
                var exceptionInfo =
                    $"Something went wrong when application was trying to send http request: {exception.Message} ApiHost: {cfg.ApiHost}";
                Log.Error(exceptionInfo);
                await SendToLocal(MakeExceptionReport(exceptionInfo).ToString(), "exception");
                return exceptionInfo;
            }
        }

        internal static async Task SendToLocal(string message, string type)
        {
            try
            {
                var port = Program.Cfg.Port;
                using var client = new HttpClient();
                var response = await client.PostAsync(new Uri($"http://localhost:{port}/{type}"),
                    new StringContent(message, Encoding.UTF8, "application/json"));
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException exception)
            {
                var exceptionInfo =
                    $"Something went wrong when application was trying to send {type} to local server: {exception.Message}";
                Log.Error(exceptionInfo);
            }
        }

        internal static JObject MakeTweetReport(dynamic tweetInfo, IEnumerable<dynamic> companies, string authorName,
            DateTime date)
        {
            dynamic report = new JObject();
            report.type = "tweet";
            report.polarity = tweetInfo.polarity;
            report.text = tweetInfo.text;
            report.companies = new JArray(companies.Select(company => new JObject
            {
                {"name", company.Name.ToString().ToLower()},
                {"price", GetStockPrice(company.NYSE.ToString()).Result.Price.ToString()}
            }));
            report.author = authorName;
            report.date = date.ToString(CultureInfo.CurrentCulture);
            return report;
        }

        internal static JObject MakeExceptionReport(string exceptionInfo)
        {
            dynamic report = new JObject();
            report.type = "exception";
            report.info = exceptionInfo;
            return report;
        }
    }
}