﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Tweetinvi;
using Tweetinvi.Models;
using Serilog;
using System.Linq;
using Tweetinvi.Core.Extensions;

namespace TweetSemantic
{
    internal static class Program
    {
        public static readonly AppConfig Cfg = AppConfig.InitOptions<AppConfig>();
        private static readonly dynamic Stocks = ReadJsonFromFile("revstocks.json");

        static Program()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File("tweetsemantic.log")
                .CreateLogger();
            Log.Debug("Application loaded.");
        }

        private static async Task Main()
        {
            await ParseTweets();
        }

        private static async Task ParseTweets()
        {
            var cfgTweetInvi = Cfg.TweetInvi;
            var credentials = new TwitterCredentials(cfgTweetInvi.ConsumerKey,
                cfgTweetInvi.ConsumerSecret,
                cfgTweetInvi.AccessToken,
                cfgTweetInvi.AccessTokenSecret);
            var client = new TwitterClient(credentials);
            var stream = client.Streams.CreateFilteredStream();
            try
            {
                cfgTweetInvi.FollowerId.ForEach(id => stream.AddFollow(id));
                stream.MatchingTweetReceived += async (sender, eventReceived) =>
                {
                    if (!cfgTweetInvi.FollowerId.Contains(eventReceived.Tweet.CreatedBy.Id) ||
                        eventReceived.Tweet.IsRetweet || eventReceived.Tweet.QuotedStatusId != null) return;
                    var tweet = eventReceived.Tweet.FullText;
                    var companies = (IEnumerable<dynamic>) Stocks.Companies;
                    var tweetReport = Requests.GetTweetPolarity(tweet).Result;
                    var result = companies.Where(company =>
                        tweet.ToLower().Contains(company.Name.ToString().ToLower()));
                    var name = eventReceived.Tweet.CreatedBy.Name;
                    var date = eventReceived.Tweet.CreatedAt.Date;
                    await Requests.SendToLocal(Requests.MakeTweetReport(tweetReport, result, name, date).ToString(),
                        "tweet");
                };

                await stream.StartMatchingAnyConditionAsync();
            }

            catch (IOException)
            {
                stream.Stop();
                await stream.StartMatchingAnyConditionAsync();
            }
            catch (Exception exception)
            {
                var exceptionInfo =
                    $"Something went wrong when application was trying to parse tweets {exception.Message}";
                Log.Error(exceptionInfo);
                await Requests.SendToLocal(Requests.MakeExceptionReport(exceptionInfo).ToString(), "exception");
                throw;
            }
        }

        private static dynamic ReadJsonFromFile(string filename)
        {
            using var file = File.OpenText(filename);
            return JsonConvert.DeserializeObject(file.ReadToEnd());
        }
    }
}