using System;
using Microsoft.Extensions.Configuration;

namespace TweetSemantic
{
    public class AppConfig
    {
        public RapidOptions RapidApi { get; set; }
        public TweetInviOptions TweetInvi { get; set; }
        public RabbitMqOptions RabbitMq { get; set; }
        public int Port { get; set; }

        private static IConfigurationRoot InitConfig()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var builder = new ConfigurationBuilder()
                .AddJsonFile($"appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env}.json", true, true)
                .AddEnvironmentVariables();

            return builder.Build();
        }

        public static T InitOptions<T>()
            where T : new()
        {
            var config = InitConfig();
            return config.Get<T>();
        }
    }
}